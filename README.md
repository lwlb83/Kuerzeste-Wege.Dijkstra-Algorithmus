# Thema Kürzeste Wege. Dijkstra-Algorithmus

## Team Philipp Merchel, Ludmilla Wilberger, Al Hajiri Fatma 

## Funktionsbeschreibung
- Suche nach kürzester Verbindung


## Bezug zu Mathematik
-Graphentheorie
-lineare Optimierung (Dijkstra-Algorithmus)

## Zeitplan
- Grobkonzept Ende Okt
- Feinkonzept Mitte/Ende Nov.
- Prototyp für Test Ende Dez.


## Zusammenarbeit
- Regelmäßige Treffen 

## Dokumentation
- Basierend auf Javadoc, Word/PowerPoint
- Sprache Deutsch


## Offene Punkte, Fragen
- Ist die kürzeste Verbindung auch die schnellste?
